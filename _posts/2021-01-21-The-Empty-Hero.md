---
layout: post
title: The Empty Hero 
---
<center>
	<h1>
		The Empty Hero
	</h1>
</center>
<br>
<h2>
	You Either Die a Hero or Live Long Enough to See Yourself Become a Villain
</h2>
<br>

One of my first serious contact with Che Guevara was through the Reddit post [Why is Fidel Castro so often seen as such a villain and Che Guevara seen as such a hero](https://www.reddit.com/r/AskHistorians/comments/254a9k/why_is_fidel_castro_so_often_seen_as_such_a/). At the time, the most voted answer, now erased, was nothing more than the Christopher Nolan’s quote "you either die a hero or live long enough to see yourself become a villain" from his 2008’s film _The Dark Knight_. From this answer, Che is classified as a hero who died fighting the capitalist oppression while Castro turned into the degenerated bureaucracy and dictator. Floating away from Che’s ideals, this view helps us to understand the commodification of Che’s image: a revolutionary who, in reality, quoting the 1965’s song _Hasta Siempre_ by Carlos Puebla, “has come as a burning breeze to plant a flag with the light of his smile”, but later was phagocytized into an empty figure that bears no relation with his former praxis, only to be sold as t-shirts, mugs and flags around the world. But the most ironic aspect of this view is that this distorted version of Che lingers on our imagination as a product to be desired whenever we have anti-capitalists thoughts. 

The founding of a socialist state within a capitalist world is not desired goal for a revolutionary heroic figure; this would sully the hero's image with bureaucratic intentions. The contradictions that comes from founding a socialist state after centuries of liberal indocrination don't sell merchandise. One cannot imagine Castro's regrets about the persecution of LGBTs in Cuba as part of revolution itself, as present in the dialetics that realises _machismo_ as one of the pillars in the capitalist structure and thus work to overcome it; this would not be cool as a scene in Walter Salles' _Diarios de motocicleta_. 

Death is an essential stage for the construction of a heroic figure.  


It's recurring theme in Nolan's filmography that an idea can overcome its idealisor's fears, weaknesses, death or defeat; a processes trhough which the idea becomes alive and start to make schemes on its own. After death, the hero becomes an idea. An idea that can no longer produce contradictions to its previous self; the perfect product.   

Quoting the Brazilian right-wing journalist Ricardo Amorim, there are only three things that functions in Cuba: Education, health and public security.  

Thus, death in this context don't mean the end. 

<br>
## The Everlasting Struggle
<br>

_Shingeki no Kyoji_ is a manga/anime series that, innitially, is settle in a world whereas mankind isolated itself inside walls to defend against the titan manance. The population inside the walls have no memories that dates prior to a century before the story starts; history was erased from their memories. The main protagonist is Eren Yeager, a boy that is constantly unquiet about the general conformoty with the life within the walls, especially with the army. He dreams about joining the Survey Corps, the branch of the army charged with ..., and wear the wings of freedom of their uniform. 

Eren's life is completely changed when the Titans attack the outer walls, breaching it and forcing the population to flee to the inner circles. During the mainhem, Eren flees to his home, witnessing the horror of the population being teared into pieces and eated by the titans. At his home, he finds his house destroyed and his mother trapped under the rubbles. 


  



 



As Mark Fisher wrote in *Capitalist Realism*,

 > The power of capitalist realism derives in part from the way that capitalism susumes and consumes all of previous history: one effect of its 'system of equivalence' which can assign all cultural objects, whether they are religious iconography, pornography, or *Das Kapital*, a monetary value.

The same system that deprives social components of the contingency power that assigns society and its components to a specific time in history can only produce heroic figures that are not longer anchored in societal values. In this sense, the heroic figure of late capitalism is empty and no more reflects the wishes, fears and dreams of the collective; it's not a coincidence that the most famous heroic figures of our time are created under copy rights and company brands, bearing logos on their chests and being managed and promoted under a business model. That's the predicment of the _Empty Hero_. 

Empty here don't mean free from ideologies. Quite the contrary, the Empty Hero, as well as any cultural product, reflects the ideology of the rulling class; but instead of being a mirror image of the bourgeoisie, this heroic figure of neoliberal capitalism is a always sentient function that acts on our values, accepting any input, and deforms it into any output that was previous designed under a specific demmand. It is not and floating signifier, but the proccess that detach our values from history and depleads them from meaning. 

this heroic figure of neoliberal capitalism is always fighting, always struggling, there are no plans to settle down, there are no dreams of utopian worlds or homes to return to. It is easier to imagine the end of the world than it is to imagine Spiderman retiring. 

As Guideon Kunda wrote in _Engineering Culture_,
> Work at Tech is experienced as making great demands on time and energy. Members describe heavy workloads, scheduling pressures, competition, and the possibility of working at home, and they percieve these factors that combine to blur the distinction between work and nonwork. 


All the signs that compose the image of Empty Hero are deprived from its signifieds; it's no longer a point in a space and time that reflects the values of the collective that gave birth to it, 

As such, each generation gets a Star Wars whose heros are built aroud the same archetypes but sold differently.  

Whenever inpersonate the Dragonborn in Skyrin, or Arthur Morgan in Red Dead Redemption 2, 

Johnny Silverhand is sentient most of the time, commenting on the actions of the player, but when Takamura asks "What are you going to ", Silverhand's abscence answers the question with the slogan of capitalist realism: there is no alternative.

The Empty Hero, despite his goals, is always an individualised figure. Even a revolutionary figure that bearies resamblances with lefticists goals, always turns his back to the collective. 

## Cyberpunk







 



