---
layout: page
title: /about
permalink: /about/
---

I'm quite bad at talking and, if we assume the Descartes' principle "I think therefore, I am", I can only exist through writing. So, this blog is my attempt to exist outside my own mind. 
